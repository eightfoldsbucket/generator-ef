

#  EightFolds project generator.

>Yeoman generator used for scaffolding common projects.

## Install Yeoman
```sh
$ npm install -g yo
```

## Install Generator
```sh
$ npm install -g git+https://eightfoldsbucket@bitbucket.org/eightfoldsbucket/generator-ef.git
```

## Sub Generators
### EDM
Run the following command to generate an EDM project
```sh
$ yo ef:edm
```

#### Supported templating engine

- [Jade](http://jade-lang.com/)
- [EJS](http://www.embeddedjs.com/)

#### Using Jade

Using Jade template engine is the recommended way of developing EDMs at Lavender, it provides the flexibility to re-use pre-built components/code. The general rule of thumb to adhere to when building EDMs via Jade is DRY (don't repeat yourself).

##### Variables
Jade templates utilises a predefined variable configuration called variables.json, every time a jade:compile task is ran, it will automatically applies all the values declared in the variables.json file. variables.json should contain repeated and ever changing properties used throughout the EDM (links, page title, etc.)

##### Mixin
Jade templates also uses a predefined set of mixins to simplify EDM development and avoid repetition. Every re-usable and tested HTML elements should be defined under mixin/common.jade, re-usable style declaration should be defined under mixin/config.jade, and mixin/stylesheet.jade should contain the global stylesheet applied to the EDM.

#### Using EJS
Documentation in progress.

### Banners
Run the following command to generate an Banner project
```sh
$ yo ef:banner
```