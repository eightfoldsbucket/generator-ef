/**
 * Created by badongers on 9/04/2016.
 */
'use strict';
(function(){
    var gulp = require('gulp');
    var gutil = require('gulp-util');
    var browserSync = require('browser-sync');
    var less = require('gulp-less');
    var autoprefix = require('gulp-autoprefixer');
    var gls = require("gulp-live-server");

    gulp.task("server", function(){
        var server = gls.static("src");
        server.start();
        gulp.watch(["src/assets/**/*.css", "src/assets/**/*.js", "src/assets/img/*", "src/**/*.html"], function(file){
            server.notify.apply(server, [file]);
        });
    });


    gulp.task('css', function () {
        return gulp.src(['./src/assets/less/*.less'])
            .pipe(less({ style: 'compressed' }).on('error', gutil.log))
            .pipe(autoprefix())
            .pipe(gulp.dest('./src/assets/css/'))
            .pipe(browserSync.stream());

    });

    gulp.task('watch', function () {
        function reportChange(event){
            console.log('Event type: ' + event.type); // added, changed, or deleted
            console.log('Event path: ' + event.path); // The path of the modified file
        }
        gulp.watch('./src/assets/less/*.less', ['css']).on('change', reportChange);
    });

    gulp.task('dev', ['css', 'watch', 'server']);

    gulp.task('default', ['dev']);
})();


