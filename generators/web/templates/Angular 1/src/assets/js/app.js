/**
 * Created by badongers on 9/04/2016.
 */
(function(){
    var appname = "myapp";
    angular.module(
        appname,
        []
    );
    angular.element(document).ready(function() {
        if(!angular.element(document).scope()){
            angular.bootstrap(document, [appname], {
                strictDi: false
            });
        }
    });
    /** REMOVE ME **/
    angular.module(appname)
        .controller("ExampleController", ["$scope", function($scope){
            $scope.title = "Angular 1 Application";
        }]);

})();