/**
 * Created by donaldmartinez on 8/04/2016.
 */
import {bootstrap}    from 'angular2/platform/browser';
import {AppComponent} from './app.component';

bootstrap(AppComponent);