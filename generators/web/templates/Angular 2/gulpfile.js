/**
 * Created by donaldmartinez on 8/04/2016.
 */
var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');


gulp.task('run', function(){
    "use strict";
    var exec = require('child_process').exec,
        child;
    var args = ["npm", "start"];
    child = exec(args.join(" "));
    process.stdout.write("Running angular 2...\n");
    child.stdout.on("data", function(data){
        process.stdout.write(data);
    });
    child.stderr.on("data", function(err){
        process.stdout.write(err);
    });
    child.on("close", function(code){
        process.stdout.write("\nProcess exited with code : "+ code+"\n");
    });

});
gulp.task('less', function() {
    gulp.watch('./assets/less/**/*.less', function(event){
        "use strict";
        gulp.src('./assets/less/*.less')
            .pipe(less({
                paths: [ path.join(__dirname, 'less') ]
            }))
            .pipe(gulp.dest('./assets/css'));
    });

});
gulp.task('default', ['less', 'run']);