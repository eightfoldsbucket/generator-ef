/**
 * Created by donaldmartinez on 29/03/2016.
 */
var fs = require("fs");
var os = require("os");
module.exports = (function(){
    "use strict";
    var generators = require('yeoman-generator');
    return generators.Base.extend({
        constructor:function(){
            generators.Base.apply(this, arguments);
        },
        initializing:function(){
          console.log("initializing");
        },
        prompting:function(){
            var done = this.async();
            this.prompt([{
                type:"list",
                name:"template",
                message:"Choose templating system",
                default:"plain",
                choices:fs.readdirSync(this.templatePath())
            },{
                type:"input",
                name:"client",
                message:"Which client?",
                default:"Lavender"
            },{
                type:"input",
                name:"livereload",
                message:"Live Reload Port",
                default:35729
            },{
                type:"input",
                name:"port",
                message:"Preview port",
                default:9002
            },{
                type:"input",
                name:"stagingFolder",
                message:"Staging URL",
                default:"//192.168.203.248/inetpub/wwwroot/"
            },{
                type:"input",
                name:"stagingDomain",
                message:"Staging Domain",
                default:"amex.lav.net.au"
            },{
                type:"input",
                name:"testDomain",
                message:"Testing Domain",
                default:"images.lav.net.au"
            },{
                type:"input",
                name:"projectFolder",
                message:"Project Folder",
                default:"123456"
            }], function(answers){
                this.log(answers.template);
                this.config = answers;
                this.config.app = "src";
                this.config.dist = "dist";
                done();
            }.bind(this));
        },
        configuring:function(){

        },
        writing:function(){
            if(this.config.template == "ejs"){
                this.fs.copy(
                    this.templatePath()+"/"+this.config.template+"/src/",
                    this.destinationPath()+"/src/"
                );
                this.fs.copyTpl(
                    this.templatePath()+"/"+this.config.template+"/Gruntfile.js",
                    this.destinationPath("Gruntfile.js"), {config:this.config}
                );
                this.fs.copy(
                    this.templatePath()+"/"+this.config.template+"/package.json",
                    this.destinationPath("package.json")
                );

            }else{
                this.fs.copyTpl(
                    this.templatePath()+"/"+this.config.template+"/",
                    this.destinationPath(), {config:this.config}
                );
            }

        },
        conflicts:function(){

        },
        install:function(){
            this.npmInstall();
        },
        end:function(){
            this.spawnCommand('grunt', ['serve']);
        }
    });

})();