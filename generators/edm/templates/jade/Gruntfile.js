// Generated on 2015-09-21 using generator-lavender 1.1.0
'use strict';

var LIVERELOAD_PORT = <%=config.livereload%>;
var lrSnippet = require('connect-livereload')({
  port: LIVERELOAD_PORT
});
var mountFolder = function(connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function(grunt) {
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  var emailClients = [
    'appmail8',
    'notes8',
    'notes85',
    'ol2000',
    'ol2002',
    'ol2003',
    'ol2007',
    'ol2010',
    'ol2011',
    'ol2013',
    'ol2015',
    'android4',
    'androidgmailapp',
    'iphone5s',
    'iphone5sios8',
    'iphone6',
    'iphone6plus',
    'ipadmini',
    'ipad',
    'gmailnew',
    'ffgmailnew',
    'chromegmailnew',
    'outlookcom',
    'ffoutlookcom',
    'chromeoutlookcom',
    'yahoo',
    'ffyahoo',
    'chromeyahoo'
  ];
  var os = require("os");

  var config = {
    app: '<%=config.app%>' || 'src',
    staging: os.platform() == "win32" ? '<%=config.stagingFolder%><%=config.stagingDomain%>/<%=config.projectFolder%>' : '<%=config.stagingFolder%><%=config.stagingDomain%>/<%=config.projectFolder%>',
    otherStaging: os.platform() == "win32" ? '<%=config.stagingFolder%><%=config.testDomain%>/<%=config.projectFolder%>' : '<%=config.stagingFolder%><%=config.testDomain%>/<%=config.projectFolder%>',
    dist: '<%=config.dist%>' || './dist',
    data: {}
  };
  grunt.initConfig({
    config:config,
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      livereload: {
        options: {
          livereload: '<%= config.livereload %>'
        },
        files: [
          '<%= config.app %>/{,*/}*.html',
          '<%= config.app %>/img/{,*/}*'
        ]
      },
      jade: {
        files: ['**/*.jade', '**/*.json'],
        tasks: ['jade:compile']
      }
    },
    connect: {
      options: {
        port: "<%=config.port%>",
        livereload: "<%=config.livereload%>",
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              connect.static(config.app)
            ];
          }
        }
      }
    },
    open: {
      server: {
        path: 'http://localhost:<%=config.port%>/index.html'
      },
      deploy: {
        path: config.staging + 'index.html'
      }
    },
    litmus: {
      account1: {
        src: ['index.min.html'],
        options: {
          username: 'clint.b@lavender.ad',
          password: 'b@ckdr0p',
          url: 'https://lavender.litmus.com',
          clients: emailClients
        }
      },
      account2: {
        src: ['index.min.html'],
        options: {
          username: 'annie.c@lavender.ad',
          password: 'b@ckdr0p',
          url: 'https://lavender2.litmus.com',
          clients: emailClients
        }
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '{,*/}img/{,*/}*.{ico,png,txt,gif,jpg}',
            '{,*/}*.html'
          ]
        }]
      },
      staging: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.dist %>',
          dest: '<%= config.staging %>',
          src: [
            '{,*/}img/{,*/}*.{ico,png,txt,gif,jpg}',
            '{,*/}*.html'
          ]
        }, {
          expand: true,
          dot: true,
          cwd: '<%= config.dist %>',
          dest: '<%= config.otherStaging %>',
          src: [
            '{,*/}img/{,*/}*.{ico,png,txt,gif,jpg}',
            '{,*/}*.html'
          ]
        }]
      }
    },
    jade: {
      options: {
        client: false,
        pretty: true,
        data: function(dest, src) {
          var arr = String(src).split("/");
          arr.pop();
          return require("./"+arr.join("/")+"/variables.json");
        }
      },
      compile: {
        files: [ {
          cwd: "./<%=config.app%>",
          src: ['**/*.jade', '!**/mixin/**'],
          dest: "./<%=config.app%>",
          expand: true,
          ext: ".html"
        } ]
      }
    },
    indexRename: {
      files: {
        src: '{,*/}*.html',
        cwd: '<%= config.dist %>'
      }
    },
    dom_munger: {
      prepare: {
        options: {
          update: {
            selector: 'a',
            attribute: 'target',
            value: '_blank'
          },
          callback: function($, file) {
            // function to check if img is a spacer or content
            function notSpacer(src) {
              if (src === 'p.gif' || src.indexOf('_p.gif') > -1 || src.indexOf('cleardot.gif') > -1) {
                return false;
              }

              return true;
            }

            var prefix = $('body').attr('data-edm-id') || '',
                imgPath = $('body').attr('data-img-path') || '',
                path = file.replace('index.html', '');

            config.data.edmId = prefix;
            config.data.fileName = file;

            $('img').each(function() {
              var $this = $(this),
                  src = ($this.attr('src')).split('/');

              src = src[src.length - 1];

              if (notSpacer(src)) {
                if (!$this.attr('alt')) {
                  throw new Error('Alt tag missing from ' + src);
                }
              }
              if (prefix) {
                src = src.replace(prefix + '_', '');
                $this.attr('src', imgPath + 'img/' + prefix + '_' + src);

                try {
                  fs.renameSync(path + 'img/' + src, path + 'img/' + prefix + '_' + src);
                } catch (err) {}
              } else {
                $this.attr('src', imgPath + 'img/' + src);
              }
            });
          }
        },
        src: '<%= config.dist %>/{,*/}*.html'
      }
    }
  });
  grunt.registerTask('build', [
    'jade:compile',
    'clean:dist',
    'copy:dist',
    'dom_munger:prepare',
    'indexRename'
  ]);
  grunt.registerTask('indexRename', 'Renaming `index.html` if `data-edm-id` is present', function() {
    var files = {
      src: '{,*/}*.html',
      cwd: config.dist
    };
    var fileList = grunt.file.expand(files, files.src);
    fileList.forEach(function(file) {
      var fcont = grunt.file.read(files.cwd + '/' + file);
      var re = /data-edm-id="([\w\W]*?)"/gi;
      var m = re.exec(fcont);

      if (m) {
        grunt.log.ok(file, '\t' + m[1]);
        fs.renameSync(files.cwd + '/' + file, files.cwd + '/' + file.replace('index', m[1] + '_index'));
      } else {
        grunt.log.warn(file, '\tdata-edm-id not found.');
      }

    });
  });
  grunt.registerTask('staging', ['build', 'clean:dist', 'copy:dist', 'copy:staging']);
  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function(target) {
    if (grunt.option('allow-remote')) {
      grunt.config.set('connect.options.hostname', '0.0.0.0');
    }
    if (target === 'dist') {
      return grunt.task.run(['build','jade:compile', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'jade:compile',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function(target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('litmus', ['jade:compile', 'litmus:account1', 'litmus:account2']);
  grunt.registerTask('default', ['clean:dist', 'copy:dist', 'copy:staging']);


};

