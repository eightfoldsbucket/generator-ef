module.exports = {
  "td-mso":function(lheight){
    "use strict";
    return "mso-line-height-rule:exactly; line-height:"+(lheight || '18px')+";";
  }
};