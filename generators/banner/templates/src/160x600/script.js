/**
 * Created by donaldmartinez on 23/02/2016.
 */

(function() {
  window.$$alone = true;
  <% if (config.bannerType.indexOf("sizmek") > -1) { %>
  function initEB() {
    if (!EB.isInitialized()) {
      EB.addEventListener(EBG.EventName.EB_INITIALIZED, function(){
        "use strict";
        initBanner('#banner', '#template');
      });
    } else {
      initBanner('#banner', '#template');
    }
  }
  window.addEventListener("load", initEB);
  <% } %>

  function assignClickTag(){
    "use strict";
    <% if (config.bannerType.indexOf("sizmek") > -1) { %>
      $(".exit").click(function(g) {
        EB.clickthrough();
        g.preventDefault();
      });
    <% } else if (config.bannerType == "standard") { %>
      $(".exit").click(function(g) {
        if (clickTag) {
          window.open(clickTag)
        } else {
          try {
            window.Enabler && Enabler.exit("default")
          } catch (f) {
            console.error("Click through error!")
          }
        }
        g.preventDefault()
      });
    <% } %>
  }
  var dynamicData, tl, frameGap = 4;

  function initBanner(banner, template){
    "use strict";
    var $banner = $(banner),
        $template = $(template);
    tl = new TimelineMax({delay:.5,ease:Strong.easeInOut});

    $banner.html($template.html());
    assignClickTag();
    TweenMax.defaultEase = Strong.easeInOut;
    <% if(config.bannerType == "sizmek dynamic") { %>
    getDynData();
    setTimeout(function(){
      "use strict";
      if(waiting){
        processMessage({data:JSON.stringify(obj)});
      }
    }, 1000);
    <% } else { %>
      configureAnimations();
      startAnimation();
    <% } %>
  }
  <% if (config.bannerType == "standard") { %>
    window.addEventListener("load", function(){
      "use strict";
      initBanner("#banner", "#template");
    });
  <% } %>
  function configureAnimations(){
    "use strict";
    //configure your timeline animation here
    /*
    tl
      .from(**something**)
      .stop() //ensure you stop it first -- it should be played on start animation
     */
  }
  function startAnimation(){
    $("#banner").show(); //show the banner
    "use strict";
    tl.play(); //play your timeline animation
  }

  <% if(config.bannerType == "sizmek dynamic") { %>
  /** Dynamic Handler **/
  // code to sit inside creative
  function renderAd(dynData) {
    applyVariables();
    configureAnimations();
    startAnimation();
  }
  function applyVariables(){
    "use strict";
    try{
      //apply variables here
    }catch(e){}

  }
  function processMessage(event) {
    try{
      var data = JSON.parse(event.data);
    }catch(err){
      data = {data:obj, type:"domainContext"};
    }
    if(data.type == "domainContext"){
      dynamicData= data.data;
      waiting = false;
      renderAd(dynamicData);
    }

  }
  if (window.addEventListener) {
    addEventListener("message", processMessage, false);
  } else {
    attachEvent("onmessage", processMessage);
  }
  var waiting = true;
  function getDynData() {
    window.top.postMessage("getDynData", "*");
  }
  /** example trigger **/
  var obj = {
    address:"134 Main Street",
    bedrooms:3,
    bathrooms:2,
    carSpaces:2,
    propImage:'http://i2.au.reastatic.net/640x480/a95fd43c53305cad0733f8ecc06849fb93df501432bb3e79ebae4efcd62efe06/main.jpg',
    nextopen:"March 22, 2016",
    viewlisting:"3123",
    isdetails:false,
    option2:true
  };
  <% } %>

})();