// Generated on <%= (new Date).toISOString().split('T')[0] %> using
'use strict';
var os = require("os");
var fs = require("fs");
module.exports = function(grunt) {

  var fs = require('fs'),
    extend = require('extend');

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Configurable paths
  var config = {
    app: '<%=config.app%>' || 'src',
    staging: os.platform() == "win32" ? '<%=config.stagingFolder%><%=config.stagingDomain%>/<%=config.projectFolder%>' : '<%=config.stagingFolder%><%=config.stagingDomain%>/<%=config.projectFolder%>',
    dist: '<%=config.dist%>' || './dist',
    data: {}
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files
    watch: {

      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= config.livereload %>'
        },
        files: [
          '<%= config.app %>/**/*/*.{jpg,html,js,scss}',
          '<%= config.app %>/**/*/sprites/{,*/}*'
        ],
        tasks: ['spriteall', 'sassall']
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: "<%=config.port%>",
        livereload: "<%=config.livereload%>",
        hostname: 'localhost',
        open:true
      },

      livereload: {
        options: {
          middleware: function(connect) {
            return [
              connect.static(config.app)
            ];
          }
        }
      },
      test: {
        options: {
          open: false,
          port: 9001,
          middleware: function(connect) {
            return [
              connect.static(config.app)
            ];
          }
        }
      },
      dist: {
        options: {
          base: '<%= config.dist %>',
          livereload: false
        }
      }
    },
    // SASS
    sass: {
      all:{
        options: {
          style: 'expanded'
        },
        files: [{
          cwd:"./src/",
          src:['**/style.scss'],
          dest:"./src/",
          expand:true,
          ext:".css"
        }]
      }
    },
    sprite: {

    },
    imagemin: {
      dist: { // Target
        options: { // Target options
          optimizationLevel: 3,
          svgoPlugins: [{
            removeViewBox: false
          }],
          use: [require('imagemin-pngquant')({
            quality: '100',
            speed: 3
          })]
        },
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: './<%=config.app%>',                   // Src matches are relative to this path
          src: ['**/sprites.{png}'],   // Actual patterns to match
          dest: './<%config.dist%>'                  // Destination path prefix
        }]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '**/*.{png,jpg,html,js,css}', '!**/sprites/**'
          ]
        }]
      },
      staging: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.dist %>',
          dest: '<%= config.staging %>',
          src: [
            '**/*.{png,jpg,html,js,css,zip}', '!**/sprites/**'
          ]
        }]
      }
    }
  });

  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function(target) {
    grunt.log.warn('Please ensure that you have png images available under banner/sprites folder.');
    if (grunt.option('allow-remote')) {
      grunt.config.set('connect.options.hostname', '0.0.0.0');
    }
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([

      'spriteall',
      'sass:all',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function(target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('sassall', [
    'newer:sass:all'
  ]);
  function confirmDirectory(base, config){
    var folders = fs.readdirSync(base);

    while(folders.length){
      var folder = folders.shift();
      var stat = fs.statSync(base+"/"+folder);

      if(folder === "sprites" && stat.isDirectory()){
        config.push({
          src: [base+'/sprites/*.png'],
          dest: base+'/sprites.png',
          destCss: base+'/_sprites.scss',
          padding: 0,
          exportOpts: {quality: 100},
          base:base
        })
      }else if(stat.isDirectory()){
        confirmDirectory(base+"/"+folder, config);
      }
    }
  }
  grunt.registerTask('spriteall', "New Sprite all", function(){
    var base = "./"+grunt.config.data.config.app;
    var config = [];
    confirmDirectory(base, config);
    while(config.length){
      var c = config.pop();
      if(!grunt.config("sprite.banner"+config.length)){
        grunt.config("sprite.banner"+config.length, c);
      }
      grunt.task.run("newer:sprite:banner"+config.length);
    }

  });
  grunt.registerTask('spritedist', "New Sprite all", function(){
    var base = "./"+grunt.config.data.config.app;
    var config = [];
    confirmDirectory(base, config);
    while(config.length){
      var c = config.pop();
      if(!grunt.config("sprite.banner"+config.length)){
        grunt.config("sprite.banner"+config.length, c);
      }
      grunt.task.run("sprite:banner"+config.length);
    }

  });

  grunt.registerTask('sassdist', [
    'sass:all'
  ]);

  grunt.registerTask('build', [
    'spritedist',
    'sassdist',
    'imagemin:dist',
    'clean:dist',
    'copy:dist'
  ]);

  grunt.registerTask('staging', [
    'build',
    'copy:staging'
  ]);

  grunt.registerTask('default', [
    'build'
  ]);
};
